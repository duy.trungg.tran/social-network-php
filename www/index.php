<?php
//this is the main page for our Bitter website, 
//it will display all tweets from those we are trolling
//as well as recommend people we should be trolling.
//you can also post a tweet from here
session_start();
include("Includes/HeaderSearch.php");
include_once "Tweet.php";
include("connect.php");
include("messageGet.php");
include_once "Tweet.php";
include_once 'User.php';

$user = new User();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Home page">
    <meta name="author" content="Duy Tran - Email: duy.trungg.tran@gmail.com">
    <link rel="icon" href="favicon.ico">

    <title>Bitter - Social Media for Trolls, Narcissists, Bullies and Presidents</title>

    <!-- Bootstrap core CSS -->
    <link href="includes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="includes/starter-template.css" rel="stylesheet">
 <!-- Bootstrap core JavaScript-->
        <script src="https://code.jquery.com/jquery-1.10.2.js" ></script>
        <script type="text/javascript" src="Includes/jquery-3.3.1.min.js"></script>

	<script>
	   //just a little jquery to make the textbox appear/disappear like the real Twitter website does
            $(document).ready(function () {
                $("#button").hide();
                $("#buttonReply").hide();
                $("#myReply").hide();
                $("#btnCancel").hide();
                $("#tweet_form").submit(function () {
                $("#button").hide();
                });



                $("#myTweet").click(function () {
                    this.attributes["rows"].nodeValue = 5;
                    $("#button").show();
                    $("#mySpan").text("");

                });//end of click event
                $("#myTweet").blur(function () {
                    $("#mySpan").text("");
                    $("#spanInfo").text("");
                });
                $(document).on('click', '.reply', function () {
                    var replyToId = $(this).attr("id");
                    $('#replyToId').val(replyToId);
                    $("#myTweet").attr("placeholder", "Reply to tweet");
                    $('#myTweet').focus();
                    $("#myTweet").attr("rows", 5);
                    $("#button").show();
                    $("#btnCancel").show();

                });
         

               
                $("#tweet_form").on('submit', function (event) {
                    event.preventDefault();
                    var form_data = $(this).serialize();
                    //alert(form_data);
                    $.ajax({//get method/function in Jquery
                        url: "Tweet_proc.php",
                        method: "POST",
                        data: form_data,
                        //dataType:"JSON",
                        success: function (data) {
                            //alert(data);
                            $("#mySpan").text(data);
                            $("#myTweet").val("");
                            $('#myTweet').focus();
                            $("#myTweet").attr("rows", "1");
                            $("#button").hide();
                            $("#myTweet").attr("placeholder", "What are you bitter about today?");
                            $('#replyToId').val('0');
                            loadTweet();
                        }
                    });//end of the get function call
                });//end ajax
  });//end of ready event handler

                
    
    
    
	</script>
  </head>

  <body>
	<BR><BR>
      <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="mainprofile img-rounded">
                        <div class="bold">
                            <?php
                            $user->getMember($_SESSION['SESS_MEMBER_ID']);
                            $user->displayUser();
                            ?>
                            <BR><BR><BR><BR><BR>
                        </div><BR><BR>
                        <div class="trending img-rounded">
                            <div class="bold">
                                Trending
                            </div>
                        </div>
				
			</div>
			<div class="col-md-6">
				<div class="img-rounded">
					  <form method="POST" id="tweet_form" name="tweet_form" >
                                <div class="form-group">
                                    <input type="hidden" name="replyToId" id="replyToId" value="0"/>
                                    <textarea class="form-control" name="myTweet" id="myTweet" rows="1" placeholder="What are you bitter about today?"></textarea>
                                    <input type="submit" name="button" id="button" value="Send" class="btn btn-primary btn-lg btn-block login-button"/>
                                </div>
                            </form>
				</div>
                            <span id="mySpan" name="mySpan"></span><br><br>
				<div class="img-rounded" >
				<!--display list of tweets here-->
                           <?php
                           date_default_timezone_set('America/Halifax');
                           if (isset($_SESSION['SESS_MEMBER_ID'])) {
                                $tweet = new Tweet();
                                $id = $_SESSION['SESS_MEMBER_ID'];
                                $tweet->displayTweet($id);
                           }
//                            date_default_timezone_set('America/Halifax');
//                              if(isset($_SESSION['SESS_MEMBER_ID'])){
//                                  $id = $_SESSION['SESS_MEMBER_ID'];
//                                               $sql = "select tweets.user_id, first_name, last_name, screen_name, tweet_text, original_tweet_id, tweets.date_created, tweets.tweet_id from users "
//                . "inner join tweets ON users.user_id = tweets.user_id "
//                . "WHERE (users.user_id in (Select follows.to_id FROM follows where follows.from_id = '$id') "
//                . "or  users.user_id = '$id' )"
//                . "and (reply_to_tweet_id=0) order by tweets.tweet_id desc limit 10";
//                 $result = mysqli_query($con, $sql);
//                  while ($tweets = mysqli_fetch_array($result)) {
//                       $tweetUserName = substr($tweets['first_name'] . ' ' . $tweets['last_name'] . ' @' . $tweets['screen_name'], 0, 22);
//                     echo '<a href="userpage.php?memId=' . $tweets['user_id'] . '" >' . $tweetUserName . '</a>' . ' '; 
//                      //date algorithm
//                       date_default_timezone_set('America/Halifax');
//                        $now = new DateTime();
//                        $tweetTime = new DateTime($tweets['date_created']);
//                        $interval = $tweetTime->diff($now);
//                         if ($interval->y > 1)
//            echo $interval->format('%y years') . " ago";
//        elseif ($interval->y > 0)
//            echo $interval->format('%y year') . " ago";
//        elseif ($interval->m > 1)
//            echo $interval->format('%m months') . " ago";
//        elseif ($interval->m > 0)
//            echo $interval->format('%m month') . " ago";
//        elseif ($interval->d > 1)
//            echo $interval->format('%d days') . " ago";
//        elseif ($interval->d > 0)
//            echo $interval->format('%d day') . " ago";
//        elseif ($interval->h > 1)
//            echo $interval->format('%h hours') . " ago";
//        elseif ($interval->h > 0)
//            echo $interval->format('%h hour') . " ago";
//        elseif ($interval->i > 1)
//            echo $interval->format('%i minutes') . " ago";
//        elseif ($interval->i > 0)
//            echo $interval->format('%i minute') . " ago";
//        elseif ($interval->s > 1)
//            echo $interval->format('%s seconds') . " ago";
//        elseif ($interval->s > 0)
//            echo $interval->format('%s second') . " ago";
//                       //end date algorithm
//                       echo '<br>';
//                       echo $tweets['tweet_text'] . '<br>'; 
//                       echo '<a href="LikeTweet.php?tweetId=' . $tweets['tweet_id'] . '"><img class="icon" src="images/like.ico"></a>';
//                        echo '<a href="Retweet.php?originalTweetId=' . $tweets['tweet_id'] . '"><img class="icon" src="images/retweet.png"></a>';
//                  echo '<hr></hr>';
//                  }
//                              }
                           ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="whoToTroll img-rounded">
				<div class="bold">Who to Troll?<BR></div>
				<!-- display people you may know here-->
				<?php 
                                $user->displayNonFollowers($_SESSION['SESS_MEMBER_ID']);
//                                $id = $_SESSION['SESS_MEMBER_ID'];
//                                    $sql = "select user_id, first_name, last_name, screen_name, profile_pic from users where user_id != $id "
//                . "and user_id NOT IN (SELECT to_id from follows where from_id=$id) order by rand() limit 3";
//        $result = mysqli_query($con, $sql);
//        while ($row = mysqli_fetch_array($result)) {
//             
//                echo '<img class="bannericons" src="images/profilepics/default.jfif">';
//            
//
//            $truncateString = substr($row['first_name'] . ' ' . $row['last_name'] . ' @' . $row['screen_name'], 0, 22);
//            echo '<a href="userpage.php?memId=' . $row['user_id'] . '" >' . $truncateString . '</a>' . '<br>';
//            echo '<a href="follow_proc.php?to_id=' . $row['user_id'] . '"><button class="followbutton" type=button>Follow</button> </a>' . '<br>';
//            echo '<hr></hr>';
//        }
				?>
				</div><BR>
				<!--don't need this div for now 
				<div class="trending img-rounded">
				© 2018 Bitter
				</div>-->
			</div>
		</div> <!-- end row -->
    </div><!-- /.container -->

	

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<!--    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
    <script src="includes/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" ></script>
  </body>
</html>
