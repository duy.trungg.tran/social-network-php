<?php
//sessions - chap 17
//the internet is stateless - each request is compelely separate and knows nohing
session_start(); // use this everytime you want to use session variables
$_SESSION["name"] = "Duy"; //this is similar to what put on logic_proc page
//default session timeout is 20mins

echo session_id() . " My session id<BR>"; //unique session id
echo session_encode() . "all my session variables<BR>"; //display all your session

echo $_SESSION["name"]. "<BR>";


//logout will look like this
session_unset(); //remove all variables from memory
session_destroy(); //kill the session completely
//redirect them back to the login page with header(location)
echo session_encode() . "all my session variables<BR>"; //display all your session

