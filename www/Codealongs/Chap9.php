<?php
    function p($mystring){
        echo $mystring . "<BR>";
    }
    
    $students = array("Nick", "Jimmy", "Johnny", "jill");
    $foundStudents = preg_grep("/J/", $students); //returns an array of matching elements
    print_r($foundStudents);
    
    $myString = "How much wood could a woodchuck chuck";
    print_r(preg_match_all("/chuck/", $myString)); //only gets the first match!
    echo "<BR>";
    
    $myString = "Tuition is $3200";
    p(preg_quote($myString));
    
    $myString = "How much wood could a woodchuck chuck";
    $newString = preg_replace("/chuck/", "HELLO", $myString);
    p($newString);
    $newString = preg_filter("/HELLO/", "chuck", $myString); //doesn't return anything
    p($newString);
    
    $myString = "How much wood could";
    $myArray = preg_split("/\|/", $myString);
    print_r($myArray);
    echo "<BR>";
    
    p(strlen($myString)); //gets the length of string
    $string1 = "HELLO";
    $string2 = "hello";
    p(strcasecmp($string1, $string2));
    
    p(strtolower($string1));
    p(ucfirst($string2)); //make the first letter upper case
    
    $myString = "Cafè Française + & ^ % $ ©";
    p(htmlentities($myString));
    
    $myString = "Billy O'donnell";
    echo addslashes($myString) . "<BR>";
    //FOR SPRINT #3 - mysqli_real_escape_string($con, $myString);
    
    $myString = "Java <BR> is <i>Cool</i>";
    p(strip_tags($myString));
    p($myString);