<?php

class Student{
    private $studentId;
    private $name;
    protected $age;
    const school = "NBCC"; //dont use $
    public static $status;
    
    public function __destruct() {
        echo "object destroyed<BR>";
    }
    public abstract function SomeMethod(); //abstract method
    public static function PrintSchool(){
        echo "NBCC<BR>";
    }
    
    public function __construct($studentId, $name, $age) {
        $this->studentId = $studentId;
        $this->name = $name;
        $this->age = $age;
    }
    
    public function getStudentId() {
        //$this is reference to the current object
        //-> means dereference the pointer  (same as ".")
        return $this->studentId;
    }

    public function getName() {
        return $this->name;
    }

    public function getAge() {
        return $this->age;
    }

    public function setStudentId($studentId) {
        $this->studentId = $studentId;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setAge($age) {
        $this->age = $age;
        return $this;
    }


}

