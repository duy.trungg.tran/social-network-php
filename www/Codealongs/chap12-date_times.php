<?php

//checkdate
$valid = checkdate(2, 29, 2020);
echo $valid . "<BR>";
setlocale(LC_ALL, "can-EN");
//%A-means day ofthe week
//%d - day of the month
//%F - full date
//%B - month
echo strftime("%A, %B %d, &Y") . "<BR>";

//times
date_default_timezone_set("America/Halifax");
echo date("h:i:sa") . "<BR>";
$dateArray = getdate(); //current timestamp
print_r($dateArray);

echo "page was last modified on " . date("F d, Y h:i:sa", getlastmod()) . "<BR>";

$dateTweeted = "2020-10-12 16:00:00";//will come from db
$now = new DateTime(); // current datetime stamp
$tweetTime = new DateTime($dateTweeted); //convert it to a datetime