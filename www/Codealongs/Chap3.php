<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
         $myName = "Duy";
        $num = 10;
        echo $myName." <br/>". $num; //concatenation is a period
        echo ++$num;
        $x ="y";
        $y="x";
        
        echo $$y . "<br>";
        print("Hello world<BR>");
        printf("HELLO %s<BR>", $myName);
        echo "HELLO" .$myName."<BR>";
        //scalar variable in php    
        $value=(int) true;
        $value = 'Hello world';
        $value = 0755;//octal
        
        echo $value . "<BR>";
        //arrays in chap 5
        $student[0] = "Jimmy";
        $student[0] = "John";
        //php is case sensitive
        $MYNAME ="Jimmy";
        $x = "5";
        $y ="10";
        echo $x + $y. "<BR>";
        echo gettype($x)."<BR>";
        //byref variables
        $x = & $y;
        $y = 500;//will also change the value of x
        echo $x. "<BR>";
        
        //constant
        const PI =3.14;
        
        $myNum=0;
        if($myNum ==0){
            echo "ZERO<BR>";
        }elseif ($myNum >0){
            echo "Greater than zero<BR>";
        }else{
            echo "less than <BR>";
        }
        //spaceship operator return 1,0 or -1 if its greater than equal to or less than
//        echo $myNum <=> $x. "<BR>";
        
        for($i=0;$i<10;$i++){
            echo $i."<BR>";
            if($i==5) break;
        }
        
        do{
            echo pow($i,2) . "<BR>";
            $i++;
        }
        while($i<10);
        ?>
        <!--short circuit tag-->
        This is <?=$myName?> sentence with my name. 
    </body>
</html>
