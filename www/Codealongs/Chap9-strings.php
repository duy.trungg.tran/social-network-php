<?php

function p($myString){
    echo $myString . "<BR>";
    
}
$students = array("Nick","Jimmy","Johny","Jill");
$foundStudents = preg_grep("/J/i", $students); //returns an array of matching elements, i : case-insensitive
print_r($foundStudents);

$myString="how would wood could a woodchuck chuck";
//preg_match("/chuck/", $myString); //only get the first match
preg_match_all("/chuck/", $myString);
echo "<BR>";

$myString = "tuition is $3200";
p(preg_quote($myString));

$myString = "how much wood could a woodchuck chuck";
echo preg_replace("/chuck/", "HELLO", $myString);

//p($newString);
//$newString = preg_filter("/chuck/", "Hello", $myString);
//p($newString);

echo "<BR>";
$myString = "how|much|wood|could";
$myArray = preg_split("/|/", $myString);
print_r($myArray);
echo "<BR>";

p(strlen($myString)); //gets the length of string
$string1 = "HELLO";
$String2 = "hello";
p(strcasecmp($String2, $string1)); //case-insensitive

p(strtok($String2, $string1));
p(ucfirst($String2)); // make the first letter upper case

$myString ="Cafe Francaise +^#*%";
p(htmlentities($myString));

$myString ="Billy O'donnell";
echo addslashes($myString)."<BR>";
//for sprint#3 - mysqli_real_escape_string($con, $myString);

$myString ="Java <BR> is <i>cool</i>";
p(strip_tags($String2));
p($myString);