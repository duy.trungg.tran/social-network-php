<?php
$firstName = $_GET["fname"];
$lastName = $_GET["lname"];
$fullName = $firstName. " ". $lastName;
$format = $_GET["format"]; //XML or JSON

if($format == "json"){
    echo json_encode(array("name"=>$fullName));
    header("content-type:application/json");
    
} else {
    header("content_type: text\xml");
    echo "<?xml version=\"1.0\"?>";
    echo "<root>";
    echo "<name>" . $fullName. "</name>";
    echo "</root>";
}

