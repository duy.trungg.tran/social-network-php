-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: bitter-duytran
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `follows`
--
CREATE DATABASE  IF NOT EXISTS `bitter-DuyTran` /*!40100 DEFAULT CHARACTER SET latin1 */;
DROP TABLE IF EXISTS `follows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `follows` (
  `follow_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  PRIMARY KEY (`follow_id`),
  KEY `FK_follows` (`from_id`),
  KEY `FK_follows2` (`to_id`),
  CONSTRAINT `FK_follows` FOREIGN KEY (`from_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FK_follows2` FOREIGN KEY (`to_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follows`
--

LOCK TABLES `follows` WRITE;
/*!40000 ALTER TABLE `follows` DISABLE KEYS */;
INSERT INTO `follows` VALUES (1,8,7),(2,7,10),(3,12,7);
/*!40000 ALTER TABLE `follows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likes` (
  `like_id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`like_id`),
  KEY `FK_tweet_id_idx` (`tweet_id`),
  KEY `FK_user_id_idx` (`user_id`),
  CONSTRAINT `FK_tweet_id` FOREIGN KEY (`tweet_id`) REFERENCES `tweets` (`tweet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (1,43,7,'2020-12-09 04:30:57'),(2,39,7,'2020-12-09 04:31:01'),(3,12,7,'2020-12-09 04:37:40'),(4,13,7,'2020-12-11 04:18:17');
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message_text` varchar(255) NOT NULL,
  `date_sent` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_toid_idx` (`id`,`from_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (7,7,10,'hihihi','2020-12-08 05:57:23'),(8,7,12,'aa','2020-12-08 06:01:37'),(9,12,7,'test sprint #6','2020-12-08 06:02:30');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tweets`
--

DROP TABLE IF EXISTS `tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tweets` (
  `tweet_id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet_text` varchar(280) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `original_tweet_id` int(11) NOT NULL DEFAULT '0',
  `reply_to_tweet_id` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tweet_id`),
  KEY `FK_tweets` (`user_id`),
  CONSTRAINT `FK_tweets` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tweets`
--

LOCK TABLES `tweets` WRITE;
/*!40000 ALTER TABLE `tweets` DISABLE KEYS */;
INSERT INTO `tweets` VALUES (1,'tweet test',7,0,0,'2020-10-29 07:01:43'),(2,'tweet test',7,0,0,'2020-10-29 07:13:11'),(3,'i feel bored today',7,0,0,'2020-10-29 15:14:26'),(4,'test tweet',7,0,0,'2020-10-29 15:32:31'),(5,'monday test tweet',8,0,0,'2020-11-02 05:31:37'),(6,'trump or biden?',8,0,0,'2020-11-02 05:32:46'),(7,'aaa',7,0,0,'2020-11-11 13:17:14'),(8,'test sprint 4',7,0,0,'2020-11-11 13:42:07'),(9,'test sprint 4',7,0,0,'2020-11-11 13:42:32'),(10,'testtest',7,0,0,'2020-11-11 13:42:41'),(11,'test sprint 4',7,9,0,'2020-11-11 13:55:16'),(12,'test sprint 4',7,11,0,'2020-11-11 13:55:21'),(13,'test sprint 4',7,12,0,'2020-11-11 14:01:37'),(14,'test reply',7,0,0,'2020-11-11 14:39:37'),(15,'aaa',7,0,0,'2020-11-11 14:43:33'),(16,'aaaa',8,0,0,'2020-11-11 15:19:39'),(17,'test reply',8,0,16,'2020-11-11 15:20:12'),(18,'aaa',8,0,0,'2020-11-11 15:35:04'),(19,'test sprint 4',8,13,0,'2020-11-11 15:36:36'),(20,'why cant post',8,0,0,'2020-11-11 15:44:31'),(21,'thursday reply',8,0,0,'2020-11-12 03:41:05'),(22,'reply done',8,0,21,'2020-11-12 03:41:18'),(23,'done',8,0,0,'2020-11-12 03:55:08'),(24,'done',8,23,0,'2020-11-12 03:59:54'),(25,'hello done',8,0,24,'2020-11-12 04:03:18'),(26,'i dont know why',8,0,25,'2020-11-12 04:04:33'),(27,'why it shows my code',8,0,25,'2020-11-12 04:05:11'),(28,'errors',8,0,0,'2020-11-12 10:49:24'),(29,'abc thursday',8,0,0,'2020-11-12 12:39:58'),(30,'abc thursday',8,29,0,'2020-11-12 12:40:06'),(31,'test already',8,0,30,'2020-11-12 12:40:13'),(32,'sprint#4',8,0,0,'2020-11-12 12:46:13'),(33,'i can reply',8,0,32,'2020-11-12 12:46:40'),(34,'reply',8,0,33,'2020-11-12 12:47:08'),(35,'reply now',8,0,34,'2020-11-12 12:47:32'),(36,'abcxyz',8,0,0,'2020-11-12 12:50:19'),(37,'aaa',8,0,0,'2020-11-12 13:55:26'),(38,'i got it',8,0,37,'2020-11-12 14:23:14'),(39,'last test',7,0,0,'2020-11-13 14:25:55'),(40,'Done',7,0,39,'2020-11-13 14:26:07'),(41,'alo',8,0,39,'2020-11-13 16:57:32'),(42,'test',8,0,0,'2020-11-13 17:01:36'),(43,'last test',7,39,0,'2020-12-09 03:56:12');
/*!40000 ALTER TABLE `tweets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `address` varchar(200) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postal_code` varchar(7) NOT NULL,
  `contact_number` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `url` varchar(50) NOT NULL,
  `description` varchar(160) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profile_pic` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'Nick','Tag','nick','$2y$10$nz9XzffE7GObw7e5NrZ3zegcPpruWpua4tYDBLMVT0DJpPakfYESG','abc','New Brunswick','123456','503123123123','testingNick@nbcc.ca','test.com','testing','test','2020-10-29 05:53:38','7_1605101352.jpg'),(8,'tester','testlname','sprint3','$2y$10$VL//xX/r9icxUvMNbPCWYe2Jf64wRhQCwwSeWblnZ7yJzTck0.6W2','Kings College Rd, Fredericto','New Brunswick','E3B2G9','1231512','testsprint@ac.ca','test.com','testing','ABC','2020-11-02 05:23:41','8_1604295510.PNG'),(9,'Duy','Tran','sprint2','$2y$10$7UDgzYwaeKgopy.6teyYmex2gwbhwgilYDEXSc1hCTSDRd9m/KWzi','744 Kings College Rd, Fredericton','New Brunswick','E3B2G9','2269781379','fatara2095@gmail.com','test.com','testing','','2020-11-07 04:01:29',NULL),(10,'Duy','Tran','test4','$2y$10$fUiLWCw2APmEkGCyjCXM3.qP3ScC8YvBd8I/aSeQfcuH8VRcLypKW','744 Kings College Rd, Fredericton','New Brunswick','E3B2G9','2269781379','fatara2095@gmail.com','','aaaa','','2020-11-07 04:05:41',NULL),(11,'Duy','Tran','dddd','$2y$10$pyX00dWyekeaAY2hELhgK.nJXtvDVCveLuVe4SeKFibtviCWIQzl2','744 Kings College Rd, Fredericton','New Brunswick','E3B2G9','2269781379','fatara2095@gmail.com','','aaa','','2020-11-07 04:11:18',NULL),(12,'sprint5','sprint5','sprint5','$2y$10$W/DUJkb8s4dZrsydg/yqCupbUQNdzYNfhPhTA8.mPeom7IJ/qWHBC','fdfb','New Brunswick','E3B2G9','123423','sprint5@gma.com','test.com','testing','aaa','2020-11-27 05:20:20',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-11  0:31:02
