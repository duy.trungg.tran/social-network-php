<?php

/**
 * Description of User
 *
 * @author DuyTran
 */
class User {
    private $userId;
    private $userName;
    private $password;
    private $fName;
    private $lName;
    private $address;
    private $province;
    private $postalCode;
    private $phone;
    private $email;
    private $dateAdded;
    private $profileImage;
    private $location;
    private $description;
    private $url;
  
     public function __set($propName, $propValue) {
        $this->$propName = $propValue;
    }

    public function __get($property) {//any attribute name, it takes some variables
        return $this->$property;
    }

    public function __construct() {
        
    }
    
     public function validateUser($userName) {
        global $con;
        $sqlSelect = "select screen_name from users where screen_name='$userName'";
        $result = mysqli_query($con, $sqlSelect);
        if (mysqli_num_rows($result) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
      public function insertUser() {
        global $con;
        $sql = "insert into users (first_name, last_name, screen_name, password, 
        address, province, postal_code, contact_number, email, url, description, location)  
              values ('$this->fName','$this->lName', '$this->userName', '$this->password', '$this->address','$this->province',
        '$this->postalCode','$this->phone','$this->email', '$this->url', '$this->description', '$this->location')";
        mysqli_query($con, $sql);
        if (mysqli_affected_rows($con) == 1) {
            $this->userId = mysqli_insert_id($result);
            return true;
        } else {
            return false;
        }
    }
    
    
        public function matchUserPass($username, $password) {
        global $con;
        $sql = "select user_id, first_name, last_name, screen_name, password, date_created, profile_pic 
        from users where screen_name = '$username' ";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) > 0) {
            while ($rs = mysqli_fetch_array($result)) {
                $this->fName = $rs['first_name'];
                $this->lName = $rs['last_name'];
                $this->userId = $rs['user_id'];
                $this->userName = $rs['screen_name'];
                $this->password = $rs['password'];
                if (is_null($rs['profile_pic'])) {
                    $this->profileImage = "default.jfif";
                } else {
                    $this->profileImage = $rs['profile_pic'];
                }

                if (password_verify($password, $this->password)) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        } else {
            return false;
        }
    }
    
      public function displayImage() {
        global $con;
        $profile_pic = "default.jfif";
        $sql = "select profile_pic, first_name, last_name from users where user_id='$this->userId'";
        $result = mysqli_query($con, $sql);
        while ($image = mysqli_fetch_array($result)) {
            $this->fName = $image['first_name'];
            $this->lName = $image['last_name'];
            if (!is_null($image['profile_pic'])) {
                $this->profileImage = $image['profile_pic'];
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
     public function searchName($keyword, $id) {
        global $con;
        $keywordL = strtolower($keyword);
        $sql = "select user_id, first_name, last_name, screen_name, profile_pic from users where user_id != $id and "
                . "(LOWER(first_name) LIKE '%" . $keywordL . "%' or LOWER(last_name) LIKE '%" . $keywordL . "%' or screen_name LIKE '%" . $keywordL . "%')";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) == 0) {
            echo 'Could not find any user with "' . $keyword . '"<br><br><hr></hr>"';
        } else {
            echo '<strong>Users found:</strong>';
            echo '<hr></hr>';
            while ($row = mysqli_fetch_array($result)) {

                $truncateString = substr($row['first_name'] . ' ' . $row['last_name'] . ' @' . $row['screen_name'], 0, 22);
                echo '<a href="userpage.php?memId=' . $row['user_id'] . '" >' . $truncateString . '</a>';
                if ($this->checkFollowing($id, $row['user_id'])) {
                    echo "|Following";
                } else {
                    echo '<a href="follow_proc.php?to_id=' . $row['user_id'] . '"><button class="followbutton" type=button>Follow</button> </a>';
                }
                if ($this->checkFollowing($row['user_id'],$id)) {//swapped parameters to check follow me
                    echo "|Follows You";
                }

                echo '<br><br>';
            }
            echo '<hr></hr>';
        }
    }
    
    public function getMember($id) {
        global $con;
        $sql = "select profile_pic, first_name, last_name, province, screen_name, date_created from users where user_id='$id'";
        $result = mysqli_query($con, $sql);
        while ($image = mysqli_fetch_array($result)) {
            $this->userId = $id;
            $this->fName = $image['first_name'];
            $this->lName = $image['last_name'];
            $this->province = $image['province'];
            $this->dateAdded = $image['date_created'];
            if (!is_null($image['profile_pic'])) {
                $this->profileImage = $image['profile_pic'];
            } else {
                $this->profileImage = "default.jfif";
            }
        }
        $sqlTweet = "select * from tweets where user_id='$id' and reply_to_tweet_id=0 ";
        $resultTweet = mysqli_query($con, $sqlTweet);
        $this->noTweets = mysqli_num_rows($resultTweet);
        $sqlFollowing = "select * from follows where from_id='$id'";
        $resultFollowing = mysqli_query($con, $sqlFollowing);
        $this->noFollowing = mysqli_num_rows($resultFollowing);
        $sqlFollowers = "select * from follows where to_id='$id'";
        $resultFollowers = mysqli_query($con, $sqlFollowers);
        $this->noFollowers = mysqli_num_rows($resultFollowers);
        
        return $this;
    }
    
        public function displayUser(){
        echo '<img class="bannericons" src="images/profilepics/' . $this->profileImage . '">';
                            echo '<a href="userpage.php?memId=' . $this->userId . '" >'  .$this->fName . ' ' . $this->lName . '</a><BR></div>';
                            echo'<table>';
                            echo '<tr><td>tweets</td><td>following</td><td>followers</td></tr>';
                            echo '<tr><td>' . $this->noTweets . '</td><td>' . $this->noFollowing . '</td><td>' . $this->noFollowers . '</td></tr>';
                            echo '</table>';
                            echo'<img class="icon" src="images/location_icon.jpg">' . $this->province
                            . '<div class="bold">Member Since:</div>
				<div>' . date('M jS, Y', strtotime($this->dateAdded)) . '</div>';
    }

        public function displayNonFollowers($id) {
        global $con;
        $sql = "select user_id, first_name, last_name, screen_name, profile_pic from users where user_id != $id "
                . "and user_id NOT IN (SELECT to_id from follows where from_id=$id) order by rand() limit 3";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result)) {
            if (isset($row['profile_pic'])) {
                echo '<img class="bannericons" src="images/profilepics/' . $row['profile_pic'] . '">';
            } else {
                echo '<img class="bannericons" src="images/profilepics/default.jfif">';
            }

            $truncateString = substr($row['first_name'] . ' ' . $row['last_name'] . ' @' . $row['screen_name'], 0, 22);
            echo '<a href="userpage.php?memId=' . $row['user_id'] . '" >' . $truncateString . '</a>' . '<br>';
            echo '<a href="follow_proc.php?to_id=' . $row['user_id'] . '"><button class="followbutton" type=button>Follow</button> </a>' . '<br>';
            echo '<hr></hr>';
        }
    }
       public function checkFollowing($yourId, $followsYouId) {
        global $con;
        $sql = "select * from follows where from_id=$yourId and to_id=$followsYouId";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) == 1) {
            return true;
        }
        return false;
    }
    
    
    public function AddMessage($from_id, $screen_name, $message_text) { //$from-id is SessionID
        global $con;
        $to_id = $this->GetUserByScreenName($screen_name);
        
        $Sqlmessage_text = mysqli_escape_string($con, $message_text);
        $sql = "insert into messages (from_id, to_id, message_text) VALUES ($from_id, $to_id, '$Sqlmessage_text')";
       
        mysqli_query($con, $sql);
        if (mysqli_affected_rows($con) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function GetUserByScreenName($screen_name) {
        global $con;
        $sql = "select user_id from users where screen_name = '$screen_name'";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                return $row['user_id'];
            }
        }
        
    }
     public function GetAllMessages($userId) {
        global $con;
        $sql = "select user_id, first_name, last_name, screen_name, message_text from users "
                . "inner join messages ON users.user_id = messages.from_id "
                . "WHERE messages.to_id = $userId "
                . " order by messages.id desc";
        $result = mysqli_query($con, $sql) or die("Error: " . mysqli_error($con));

        while ($message = mysqli_fetch_array($result)) {
            $messageUserName = substr($message['first_name'] . ' ' . $message['last_name'] . ' @' . $message['screen_name'], 0, 22);
            echo '<a href="userpage.php?memId=' . $message['user_id'] . '" >' . $messageUserName . '</a>' . ' <br>';
            echo $message['message_text'];
            echo '<hr></hr>';
        }
    }

    public function GetUser($keywordL, $id) {
        global $con;
        $usersArray = array();
        $sql = "select screen_name from users where user_id in (select to_id from follows where from_id=$id) "
                . "and screen_name LIKE '%" . $keywordL . "%'";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                array_push($usersArray, $row["screen_name"]);
            }
            return json_encode($usersArray);
        }
    }
    
    
    
     Public function getNoSameFollowers($id, $loggedId) {
        global $con;
        $sql = "select user_id, first_name, last_name, screen_name, profile_pic from users where user_id != $id and user_id != $loggedId "
                . "and user_id IN (SELECT to_id from follows where from_id=$id) "
                . "and user_id IN (SELECT to_id from follows where from_id=$loggedId) order by rand()";
        
        $result = mysqli_query($con, $sql);
        $this->noSameFollowers = mysqli_num_rows($result);
        echo $this->noSameFollowers; 
        echo '&nbsp;Followers you know<BR>';
        $i=1;                       
        while ($row = mysqli_fetch_array($result)) {
            if($i>3) break;
            if (isset($row['profile_pic'])) {
                echo '<img class="bannericons" src="images/profilepics/' . $row['profile_pic'] . '">';
            } else {
                echo '<img class="bannericons" src="images/profilepics/default.jfif">';
            }
            $truncateString = substr($row['first_name'] . ' ' . $row['last_name'] . ' @' . $row['screen_name'], 0, 22);
            echo '<a href="userpage.php?memId=' . $row['user_id'] . '" >' . $truncateString . '</a>' . '<br>';

            echo '<hr></hr>';
            $i++;
        }
    }
    
    Static function Fedex($postalCode, $province) {

        function getProvinceCode($details, $spacer) {
            foreach ($details as $key => $value) {
                if (is_array($value) || is_object($value)) {
                    $newSpacer = $spacer . '&nbsp;&nbsp;&nbsp;&nbsp;';
              
                    getProvinceCode($value, $newSpacer);
                } elseif (empty($value)) {
              
                    return "";
                } else {
           
                    if (strcasecmp($key, 'StateOrProvinceCode') == 0) {
                        return $value;
                    }
                }
            }
        }

        function getProvince($code) {
            switch ($code) {
                case "NB":
                    return "New Brunswick";
                case "NF":
                    return "Newfoundland and Labrador";
                case "PE":
                    return "Prince Edward Island";
                case "NS":
                    return "Nova Scotia";
                case "PQ":
                    return "Quebec";
                case "ON":
                    return "Ontario";
                case "MB":
                    return "Manitoba";
                case "SK":
                    return "Saskatchewan";
                case "AB":
                    return "Alberta";
                case "BC":
                    return "British Columbia";
                case "YT":
                    return "Yukon";
                case "NT":
                    return "Northwest Territories";
                case "NT"://NU
                    return "Nunavut";
            }
        }

        require_once('includes/Fedex/fedex-common.php');

        $newline = "<br />";
//Please include and reference in $path_to_wsdl variable.
        $path_to_wsdl = "includes/Fedex/wsdl/CountryService/CountryService_v5.wsdl";


        ini_set("soap.wsdl_cache_enabled", "0");

        $client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

        $request['WebAuthenticationDetail'] = array(
            'ParentCredential' => array(
                'Key' => getProperty('parentkey'),
                'Password' => getProperty('parentpassword')
            ),
            'UserCredential' => array(
                'Key' => getProperty('key'),
                'Password' => getProperty('password')
            )
        );

        $request['ClientDetail'] = array(
            'AccountNumber' => getProperty('shipaccount'),
            'MeterNumber' => getProperty('meter')
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Validate Postal Code Request using PHP ***');
        $request['Version'] = array(
            'ServiceId' => 'cnty',
            'Major' => '5',
            'Intermediate' => '0',
            'Minor' => '1'
        );
//change
        $request['Address'] = array(
            'PostalCode' => $postalCode, //from user input
            //'StateOrProvinceCode' => getCode($province),
            'CountryCode' => 'CA'
        );

        $request['CarrierCode'] = 'FDXE';

        try {
            if (setEndpoint('changeEndpoint')) {
                $newLocation = $client->__setLocation(setEndpoint('endpoint'));
            }

            $response = $client->validatePostal($request);



            if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
 
                $details = $response->PostalDetail;
                $provinceCode = getProvinceCode($details, "");
        
                $provinceResponse = getProvince($provinceCode);
        
                if (strcasecmp($provinceResponse, $province) == 0) {
                    return true;
                } else {
                    //echo '<script>alert("compare");</script>';
                    return false;
                }
            } else {
                //printError($client, $response);
                //echo '<script>alert("incorrect postalcode");</script>';
                return false;
            }

            //writeToLog($client);    // Write to log file   
        } catch (SoapFault $exception) {
 
            return false;
        }


    }
}
