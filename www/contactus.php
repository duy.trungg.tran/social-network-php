<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Contact Us">
    <meta name="author" content="Duy Tran - Email: duy.trungg.tran@gmail.com">
    <link rel="icon" href="favicon.ico">

    <title>Contact Us - Bitter</title>

    <!-- Bootstrap core CSS -->
    <link href="includes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="includes/starter-template.css" rel="stylesheet">
	<!-- Bootstrap core JavaScript-->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	
    <script src="includes/bootstrap.min.js"></script>
    
	<script type="text/javascript">
		//any JS validation you write can go here
	</script>
  </head>

  <body>

    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<a class="navbar-brand" href="index.html"><img src="images/logo.jpg" class="logo"></a>
		
        
      </div>
    </nav>

	<BR><BR>
    <div class="container">
		<div class="row">
			
			<div class="main-login main-center">
                            <h1>Contact Us - We 'd love to hear from you!</h1>
                            <p>Email: <a href="mailto:duy.trungg.tran@gmail.com"> duy.trungg.tran@gmail.com</a> </p>
                            <p>Address: <a href="https://www.google.com/maps/place/744+Kings+College+Rd,+Fredericton,+NB+E3B+2G9,+Canada/@45.9470278,-66.6480291,17z/data=!3m1!4b1!4m5!3m4!1s0x4ca42211cfaf2ee3:0xd7ef96d992a5c995!8m2!3d45.9470278!4d-66.6458404">
                        744 Kings College Road, Fredericton, New Brunswick, E3B 2G9.</a></p>
                            <p>Phone: 226-978-1379</p>
                            
				</div>
			
		</div> <!-- end row -->
    </div><!-- /.container -->
    
  </body>
</html>